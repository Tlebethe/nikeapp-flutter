import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../model/cart_model.dart';

class ProductDetailScreen extends StatefulWidget {
  final Map<String, dynamic> productData;
  const ProductDetailScreen({super.key, required this.productData});

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    size: 30,
                  )),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.75,
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white12,
                      width: MediaQuery.of(context).size.width,
                      height: 350,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.productData['images'].length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: Image.network(
                                widget.productData['images'][index],
                                fit: BoxFit.cover,
                              ),
                            );
                          }),
                    ),
                    //title
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      widget.productData['name'],
                      style: const TextStyle(
                        fontSize: 35,
                        color: Colors.black87,
                      ),
                    ),
                    //price
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      'US\$ ${widget.productData['price'].toString()}',
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black54,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    //sizes
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      'Available sizes:  ${widget.productData['sizes'].toString()}',
                      style:
                          const TextStyle(fontSize: 17, color: Colors.black45),
                    ),
                    //description
                    const SizedBox(
                      height: 40,
                    ),
                    Text(
                      widget.productData['description'],
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black87,
                      ),
                    )
                  ],
                )),
              ),
              //the checkout button
              Consumer<CartModel>(
                builder: (context, cart, child) => InkWell(
                  splashColor: Colors.black,
                  onTap: () {
                    cart.addItemsToCart({
                      'product': {
                        "id": widget.productData['_id'],
                        "image": widget.productData['image'],
                        "name": widget.productData['name'],
                        "price": widget.productData['price'],
                      },
                      'size': 32,
                      'quantity': 1,
                    });
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.black87,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(20),
                    child: const Text(
                      'Add to cart',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
