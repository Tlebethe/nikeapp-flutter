import 'package:flutter/material.dart';
import 'package:nikeapp/screens/cart_screen.dart';
import 'package:nikeapp/screens/order_tracker.dart';

import 'package:nikeapp/screens/product_detail_screen.dart';
import 'package:provider/provider.dart';
import '../model/cart_model.dart';
import '../nikeapi/api_nike.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({super.key});

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  List products = [];
  late Future<Products> producstList;

  @override
  void initState() {
    super.initState();
    producstList = fetchProducts();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Nike Store'),
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const OrdersScreen(),
                ),
              );
            },
            icon: const Icon(Icons.checklist_rtl_rounded)),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const Cart()));
                    },
                    icon: const Icon(
                      Icons.shopping_cart_sharp,
                      size: 30,
                    )),
                Consumer<CartModel>(
                    builder: (context, cart, child) =>
                        Text(cart.cartCount.toString()))
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
        future: producstList,
        builder: ((context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (snapshot.hasData) {
            return GridView.count(
              crossAxisCount: 2,
              children: List.generate(snapshot.data!.data.length, (index) {
                return Padding(
                  padding: const EdgeInsets.all(5),
                  child: InkWell(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ProductDetailScreen(
                          productData: snapshot.data!.data[index],
                        ),
                      ))
                    },
                    child: Image.network(snapshot.data!.data[index]['image']),
                  ),
                );
              }),
            );
          }
          return const CircularProgressIndicator();
        }),
      ),
    );
  }
}
