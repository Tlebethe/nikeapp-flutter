import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../model/cart_model.dart';
import '../widgets/utils.dart';

class Cart extends StatefulWidget {
  const Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  late String refId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                onPressed: () => Navigator.of(context).pop(),
                icon: const Icon(
                  Icons.arrow_back,
                  size: 30,
                ),
              ),
              Consumer<CartModel>(
                builder: (context, cart, child) => Expanded(
                  child: cart.cartItems.isNotEmpty
                      ? ListView.builder(
                          itemCount: cart.getCart.length,
                          itemBuilder: (context, index) {
                            return cartItem(
                              cart.getCart[index]['product']['image']
                                  .toString(),
                              cart.getCart[index]['product']['name'].toString(),
                              cart.getCart[index]['quantity'],
                              cart.getCart[index]['product']['price'],
                              context,
                              index,
                            );
                          })
                      : const Center(
                          child: Text('Shop to add Items to cart...'),
                        ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              //cost
              Row(
                children: [
                  const Text(
                    'SubTotal: ',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Consumer<CartModel>(
                    builder: (context, cart, child) => Text(
                      'US\$${cart.subTotal()}',
                      style: const TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              //delivery fee
              Row(
                children: [
                  const Text(
                    'Delivery fee: ',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Consumer<CartModel>(
                    builder: (context, cart, child) => Text(
                      'US\$${cart.getDeliveryFee}',
                      style: const TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              //total cost
              Row(
                children: [
                  const Text(
                    'Total Cost: ',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Consumer<CartModel>(
                    builder: (context, cart, child) => Text(
                      'US\$${cart.totalCost()}',
                      style: const TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              //checkout button
              Consumer<CartModel>(
                builder: (context, cart, child) => cart.cartItems.isNotEmpty
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: InkWell(
                          splashColor: Colors.amber,
                          onTap: () async {
                            var data = await cart.checkOut();
                            if (data.statusCode == 200) {
                              refId = jsonDecode(data.body)['data']['result'];
                            }
                            //print('data in ui ${data}');
                            if (mounted) {
                              showRefrenceData(context, refId);
                            }
                          },
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Colors.black,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.all(20),
                            child: const Text(
                              'Checkout',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      )
                    : const Text(''),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
