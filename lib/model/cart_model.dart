import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:nikeapp/nikeapi/api_nike_orders.dart';

class CartModel extends ChangeNotifier {
  final List cartItems = [];

  int deliveryFee = 15;
  num subtotal = 0;
  num total = 0;

  List get getCart => cartItems;
  int get getDeliveryFee => deliveryFee;
  int get cartCount => cartItems.length;

  void addItemsToCart(dynamic item) {
    if (cartItems.isEmpty) {
      cartItems.add(item);
    } else if (cartItems.isNotEmpty &&
        cartItems.firstWhere(
                (element) => element['product']['id'] == item['product']['id'],
                orElse: () => false) !=
            false) {
      item = cartItems.firstWhere(
          (element) => element['product']['id'] == item['product']['id']);
      item['quantity'] += 1;
    } else {
      cartItems.add(item);
    }
    notifyListeners();
  }

  void increaseQuantity(int index) {
    cartItems[index]['quantity'] += 1;
    notifyListeners();
  }

  void decreaseQuantity(int index) {
    if (cartItems[index]["quantity"] <= 1) {
      cartItems.remove(cartItems[index]);
    } else {
      cartItems[index]["quantity"] -= 1;
    }
    notifyListeners();
  }

  num subTotal() {
    if (cartItems.isEmpty) {
      subtotal = 0;
    } else {
      subtotal = 0;
      for (int i = 0; i < cartItems.length; i++) {
        subtotal += cartItems[i]['product']['price'] * cartItems[i]['quantity'];
      }
    }
    return subtotal;
  }

  num totalCost() {
    total = 0;
    total = subTotal() + deliveryFee;
    return total;
  }

  Future<Response> checkOut() async {
    //checking out items
    var data = await createorder({
      'items': cartItems,
      'subtotal': subtotal,
      'delivery-fee': deliveryFee,
      'total': total,
      'customer': {
        'name': 'Thato',
        'email': 't@gmail.com',
      }
    });
    cartItems.clear();
    notifyListeners();
    return data;
  }
}
