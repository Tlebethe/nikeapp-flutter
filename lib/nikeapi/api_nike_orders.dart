import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

Future<http.Response> createorder(Map<String, dynamic> order) {
  String platformUrl = Platform.isAndroid
      ? 'http://10.0.2.2:4000/orders'
      : 'http://localhost:4000/orders';
  return http.post(
    Uri.parse(platformUrl),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, Map<String, dynamic>>{
      'order': order,
    }),
  );
}
