import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

Future<http.Response> connectToApi() {
  return http.get(Uri.parse('http://localhost:8000/products'));
}

class Products {
  final String status;
  final List<dynamic> data;

  const Products({
    required this.status,
    required this.data,
  });

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
      status: json['status'],
      data: json['data'],
    );
  }
}

Future<Products> fetchProducts() async {
  String platformUrl = Platform.isAndroid
      ? 'http://10.0.2.2:4000/products'
      : 'http://localhost:4000/products';
  final response = await http.get(Uri.parse(platformUrl));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return Products.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
