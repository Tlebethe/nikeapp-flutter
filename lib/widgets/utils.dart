import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../model/cart_model.dart';

Widget cartItem(
  String image,
  String name,
  int quantity,
  int price,
  BuildContext context,
  int index,
) {
  return Card(
    elevation: 3.0,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: 150,
          height: 150,
          child: Image.network(
            image,
            fit: BoxFit.fitWidth,
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const SizedBox(
                height: 5,
              ),
              Text(
                name,
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Consumer<CartModel>(
                    builder: (context, cart, child) => IconButton(
                      onPressed: () => {
                        cart.increaseQuantity(index),
                      },
                      icon: const Icon(Icons.add),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(quantity.toString()),
                  const SizedBox(
                    width: 20,
                  ),
                  Consumer<CartModel>(
                    builder: (context, cart, child) => IconButton(
                      onPressed: () => {cart.decreaseQuantity(index)},
                      icon: const Icon(Icons.remove),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Text('Price: '),
                  Text('US \$ ${price * quantity}'),
                ],
              ),
            ],
          ),
        )
      ],
    ),
  );
}

Future<String?> showRefrenceData(BuildContext context, String refId) {
  return showDialog<String>(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: const Text('Thank you for shopping...'),
      content: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Please keep your reference ID safe.'),
            Text(
              'Your ref Id: $refId',
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 20,
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'OK'),
          child: const Text('OK'),
        ),
      ],
    ),
  );
}
