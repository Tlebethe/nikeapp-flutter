<h1>Nike ecommerce app with Flutter</h1>

<p>This is a sneaker ecommerce app for nike shoes</p>

<h2> Features...</h2>

- view a grid of shoes

- click on a specific shoe

- when a user clicks on a specific shoe
user is direccted to a screen displaying  details about the specific shoes i.e details, price, shoes size available

- on the shoe details screen there is a buttton that allows the user to add that specific shoe to their cart

- on the homescreen there is a cart page that calculates
the total price of all shoes combined

<h2>Tech Stack..</h2>

-   frontend (client) -  Flutter
-   backend  (server and database)- Nodejs and MongoDB (please see nikeapp backend repo  for more)

<h2>Installation</h2>

After cloning this repository, migrate to nikeappflutter folder. Then, follow the following steps:

<b>NOTE!!! If you havent already you will first need to fireup the Nodejs
server , the Clinet needs the server to start up. Please check out nikeapp backend repo for more...</b>


Run the following commands to run your app:
  <ul>flutter pub get</ul>
  <ul>open -a simulator (to get iOS Simulator)</ul>
  <ul>flutter run</ul>

<h1>Feedback</h1>
  <p>if you have any feedback please feel free to contact me on
thatofreelance@gmail.com
  </p>
  

<h2>Why did I build this project?</h2>
<p>I had just finished the freecodecamp course on Back End Development and APIs</p>
<p>I wanted to build an ecommerce store since i felt that would be a great challenge to code both the backend and frontend not to mention the added advantage of learning how to build an ecommerce app.</p>
<h2>What was your motivation?</h2>
<p>Reinforce the what I had learned in freecodecamps backend course </p>
<p>Create a minimalistic UI for the client that is easy to use</p>

<h2>What did you learn?</h2>
<li>
<b>Client UI</b>
<ul>Improved my flutter skills</ul>
<ul>Using Flutter provider package more  efficiently</ul>
<b>Server backend</b>
<ul>NodeJs</ul>
<ul>MongoDB</ul>
<ul>NPM packages</ul>
</li>

<h2>Screenshots</h2>

<div style="width:55px; height: 55px">![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.46.06.png)</div>
  
![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.46.11.png)

![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.46.20.png)


![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.46.32.png)

![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.46.38.png)

![image info](project screenshots/Simulator Screen Shot - iPhone 14 Pro Max - 2023-03-28 at 10.47.01.png)





